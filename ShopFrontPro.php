<?php

class StringToArray	{
	
	public static function generateArray($strings)	{
		$array = [];
		
		foreach (static::sort($strings) as $string)	{
			$values = explode('/', $string);
			
			static::generate($array, $values, count($values));
		}
		
		return $array;
	}
	
	public static function generatePaths($base, $length, $depth, $leaves)	{
		$paths = [];
		
		for ($counter = 0; $counter < $length; $counter ++)
			$paths[] = [
				'path' => static::getRandomPath($paths, $depth, $leaves),
				'name' => static::getRandomFile() . '.txt'
			];
		
		for ($counter = 0; $counter < $length; $counter ++)
			$paths[$counter] = $base . implode('/', $paths[$counter]['path']) . '/' . $paths[$counter]['name'];
			
		return $paths;
	}
	
	
	
	public static function convert($array, $parent = '')	{
		$tree = [];
		
		foreach ($array as $key => $value)	{
			if (is_array($value))
				$tree[$key] = [
					'parent' => $parent,
					'children' => static::convert($value, $key)
				];
			else
				$tree[$key] = [
					'parent' => $parent,
					'children' => $value
				];
		}
		
		return $tree;
	}
	
	public static function printArray($array, $depth = 1, $leaf = 1, $indent = 0)	{
		$counter = 0;
		
		if ($depth > 0)
			foreach ($array as $key => $value)	{
				if (is_array($value))	{
					echo str_pad('', $indent * 4, ' ') . "$key\n";
					
					static::printArray($value, $depth - 1, $leaf, $indent + 1);
				} else if ($counter < $leaf)	{
					echo str_pad('', $indent * 4, ' ') . "$value\n";
				
					$counter ++;
				}
			}
	}
	
	
	
	private static function sort($array)	{
		usort($array, function($a, $b)	{
			return strlen($a) > strlen($b) ? 1 : -1;
		});
		
		return $array;
	}
	
	private static function generate(&$array, $values, $length, $index = 0)	{
		if ($values[$index] === '')
			static::generate($array, $values, $length, $index + 1);
		else if ($index + 1 < $length)	{
			if (!isset($array[$values[$index]]))
				$array[$values[$index]] = [];
			
			static::generate($array[$values[$index]], $values, $length, $index + 1);
		} else
			$array[] = $values[$index];
	}
	
	
	
	private static function getRandomPath($paths, $depth, $leaves)	{
		$path = [];
		$count = rand(1, $depth);
		$pathcount = 0;
		
		foreach ($paths as $row)
			if (count($row['path']) === $count)
				$pathcount ++;
			
		if ($pathcount === $leaves)
			return static::getRandomPath($paths, $depth, $leaves);
			
		for ($counter = 1; $counter <= $count; $counter ++)
			$path[] = "folder$counter";
		
		return $path;
	}
	
	private static function getRandomFile()	{
		$chars = array_merge(range('a', 'z'), range(0, 9));
		$name = '';
		
		for ($counter = 0; $counter < 20; $counter ++)
			$name .= $chars[rand(0, count($chars) - 1)];
		
		return $name;
	}
	
}

$array = StringToArray::generateArray([
	'/home/user/folder1/folder2/kdh4kdk8.txt',
	'/home/user/folder1/folder2/565shdhh.txt',
	'/home/user/folder1/folder2/folder3/nhskkuu4.txt',
	'/home/user/folder1/iiskjksd.txt',
	'/home/user/folder1/folder2/folder3/owjekksu.txt',
]);



print_r($array);
print_r(StringToArray::convert($array));
StringToArray::printArray($array, 6, 1);
print_r(StringToArray::generatePaths('/home/user/', 5, 5, 1));
